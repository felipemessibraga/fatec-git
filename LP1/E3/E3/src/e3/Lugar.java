package e3;


public class Lugar {
    public String nome;
    private String cidade;
    private String pais;
    private String descricao;
    
    public void status(){
        System.out.printf("Nome: %s", this.getNome());
        System.out.printf("\nCidade: %s", this.getCidade());
        System.out.printf("\nPais: %s", this.getPais());
        System.out.printf("\nDescricao: %s", this.getDescricao());
        System.out.println("\n----------------------\n");
    }

    private void endereco(){
        System.out.printf("Esse lugar fica em %s \nno pais: %s\n", this.getCidade(), this.getPais());
    }
    
    public void sobre(){
        switch(this.getNome()){
            case "Cristo Redentor":
                this.setDescricao("O Cristo redentor fica no Rio de Janeiro");
                System.out.println(getDescricao());
                break;
            case "MASP":
                this.setDescricao("O MASP é um prédio de SP famoso pelo VãoLivre");
                System.out.println(getDescricao());
                break;
            default:
                endereco();
                break;
        }
        
    }

    public Lugar(String nome, String cidade, String pais) {
        this.nome = nome;
        this.cidade = cidade;
        this.pais = pais;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
}
