package e3;


public class Profissional {
    public String profissional;
    private int idade;
    private boolean quarentena;
    
    public void status(){
        System.out.printf("Profissional: %s\n", this.getProfissional());
        System.out.printf("Idade: %d\n", this.getIdade());
        System.out.printf("Está tendo quarentena? %b", this.isQuarentena());
        this.risco();        
    }
    
    protected void risco(){
        if (this.getIdade()> 60 && this.isQuarentena()){
            System.out.println("Grupo de Risco, o profissional deve se resguardar");
            ajudar(false);
        }else{
            if (this.isQuarentena()){
                System.out.println("Profissional, procure saber como ajudar na quarentena! Precisamos de Todos!");
                ajudar(true);
            }else{
                System.out.println("Estamos fora da Quarentena!");
                ajudar(false);
            }
        }
    }
    
    protected void ajudar(boolean ajuda){
        if (ajuda){
            System.out.println("É de extrema importância que ajudemos uns aos outros nesse momento tão difícil!");
        }else{
            System.out.println("Cuidados de higiêne e saúde devem ser SEMPRE TOMADOS! Qualquer dúvida entre em contato com a autoridade sanitária local.");
        }
    }

    public Profissional(String profissional, int idade, boolean quarentena) {
        this.profissional = profissional;
        this.idade = idade;
        this.quarentena = quarentena;
    }

    public String getProfissional() {
        return profissional;
    }

    public void setProfissional(String profissional) {
        this.profissional = profissional;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public boolean isQuarentena() {
        return quarentena;
    }

    public void setQuarentena(boolean quarentena) {
        this.quarentena = quarentena;
    }


    
}
