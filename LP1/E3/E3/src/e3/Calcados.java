package e3;


public class Calcados {
    private String marca;
    private int tamanho;
    private String tipo;
    protected boolean calcado;
    
    public void meuSapato(){
        System.out.printf("Marca: %s\n", this.getMarca());
        System.out.printf("Tamanho: %d\n", this.getTamanho());
        System.out.printf("Tipo: %s\n", this.getTipo());
    }
    
    public void descalcar(){
        this.setCalcado(true);
        System.out.println("Sapato calçado!");
    }
    
    public void calcar(){
        this.setCalcado(false);
        System.out.println("Sapato descalçado!");
    }

    public Calcados(String marca, int tamanho, String tipo) {
        this.marca = marca;
        this.tamanho = tamanho;
        this.tipo = tipo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getTamanho() {
        return tamanho;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean isCalcado() {
        return calcado;
    }

    public void setCalcado(boolean calcado) {
        this.calcado = calcado;
    }
    
}
