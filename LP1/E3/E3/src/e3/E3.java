package e3;

import java.util.ArrayList;


public class E3 {
    
    
    public static void main(String[] args) {
        /*
        // LUGAR
        ArrayList <Lugar> lugar;
        lugar = new ArrayList();
        Lugar l1 = new Lugar("Cristo Redentor","Rio de Janeiro", "Brasil");
        Lugar l2 = new Lugar("MASP", "São Paulo", "Brasil");
        Lugar l3 = new Lugar("Museu do Ipiranga", "São Paulo","Brasil");
        Lugar l4 = new Lugar("Plano Piloto", "Brasilia","Brasil");
        Lugar l5 = new Lugar("Cataratas do Iguaçu","Foz do Iguaçu", "Brasil");
        lugar.add(l1);
        lugar.add(l2);
        lugar.add(l3);
        lugar.add(l4);
        lugar.add(l5);
              
        for (int i = 0; i < lugar.size() ; i++){
            Lugar teste = lugar.get(i);
            teste.sobre();
            teste.status();
        }
        // Fim LUGAR
        */

        ArrayList <AlunoFatec> alunoFatec;
        alunoFatec = new ArrayList();
        alunoFatec p1 = new Lugar("Cristo Redentor","Rio de Janeiro", "Brasil");
        alunoFatec p2 = new Lugar("MASP", "São Paulo", "Brasil");
        alunoFatec p3 = new Lugar("Museu do Ipiranga", "São Paulo","Brasil");
        alunoFatec p4 = new Lugar("Plano Piloto", "Brasilia","Brasil");
        alunoFatec p5 = new Lugar("Cataratas do Iguaçu","Foz do Iguaçu", "Brasil"); 
        alunoFatec.add(p1);
        alunoFatec.add(p2);
        alunoFatec.add(p3);
        alunoFatec.add(p4);
        alunoFatec.add(p5);
        
        for (int i = 0; i < alunoFatec.size() ; i++){
            AlunoFatec teste = alunoFatec.get(i);
            teste.terminarSemestre();
            teste.status();
        }
        
        
        ArrayList <InstrumentoMusical> instrumentMusical;
        instrumentMusical = new ArrayList();
        instrumentMusical im1 = new Lugar("Violão","Cordas");
        instrumentMusical im2 = new Lugar("Violão","Cordas");
        instrumentMusical im3 = new Lugar("Violão","Cordas");
        instrumentMusical im4 = new Lugar("Violão","Cordas");
        instrumentMusical im5 = new Lugar("Violão","Cordas"); 
        instrumentMusical.add(im1);
        instrumentMusical.add(im2);
        instrumentMusical.add(im3);
        instrumentMusical.add(im4);
        instrumentMusical.add(im5);
        
        for (int i = 0; i < instrumentMusical.size() ; i++){
            InstrumentoMusical teste = instrumentMusical.get(i);
            teste.tocar();
            teste.aprender();
            teste.live();
        }        
        
        ArrayList <Profissional> profissional;
        profissional = new ArrayList();
        profissional p1 = new Lugar("Medico", 57, true);
        profissional p2 = new Lugar("Medico", 57, true);
        profissional p3 = new Lugar("Medico", 57, true);
        profissional p4 = new Lugar("Medico", 57, true);
        profissional p5 = new Lugar("Medico", 57, true); 
        profissional.add(p1);
        profissional.add(p2);
        profissional.add(p3);
        profissional.add(p4);
        profissional.add(p5);
        
        for (int i = 0; i < profissional.size() ; i++){
            Profissional teste = profissional.get(i);
            teste.risco();
            teste.status();
        }        
        
        ArrayList <Calcados> calcados;
        calcados = new ArrayList();
        calcados p1 = new Lugar("Nike", 35, "Corrida");
        calcados p2 = new Lugar("Nike", 35, "Corrida");
        calcados p3 = new Lugar("Nike", 35, "Corrida");
        calcados p4 = new Lugar("Nike", 35, "Corrida");
        calcados p5 = new Lugar("Nike", 35, "Corrida"); 
        calcados.add(p1);
        calcados.add(p2);
        calcados.add(p3);
        calcados.add(p4);
        calcados.add(p5);

        for (int i = 0; i < calcados.size() ; i++){
            Calcados teste = calcados.get(i);
            teste.meuSapato();
            teste.calcar();
            teste.descalcar();
        }
        
    }
    
}
