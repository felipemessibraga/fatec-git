
package e3;


public class InstrumentoMusical {
    public String instrumento;
    private String categoria;
    
    public void tocar(){
        System.out.println("Ba tu tiss, tantanta, larararaaaaaa");
    }
    
    public void aprender(){
        System.out.println("Espero que você seja do tipo 'Pessoa talentosa' ou eu não quero ser seu vizinho! rsrs");
        System.out.println("Dedique-se!");
    }
    
    public void live(){
        System.out.println("Preparar, apontar, cuidado com a Pandemia e GO LIVE!!!");
        System.out.println("Espero que você goste de sertanejo");
    }

    public InstrumentoMusical(String instrumento, String categoria) {
        this.instrumento = instrumento;
        this.categoria = categoria;
    }

    public String getInstrumento() {
        return instrumento;
    }

    public void setInstrumento(String instrumento) {
        this.instrumento = instrumento;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
    
    
}
