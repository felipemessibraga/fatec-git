package e3;


public class Pessoas {
    public String nome;
    private boolean pandemia;
    private int idade;
    private boolean doenca;
    private boolean grupo_de_risco;
    
    public void status(){
        System.out.printf("Nome: %s", this.getNome());
        System.out.printf("\nCidade: %s", this.isPandemia());
        System.out.printf("\nPais: %s", this.getIdade());
        System.out.printf("\nDescricao: %s", this.isDoenca());
        this.risco();
        System.out.println("----------------------\n");
    }
    
    private void risco(){
        if (this.getIdade()>60 && this.isPandemia() || this.isDoenca()){
            System.out.printf("O(A) %s, pertence ao grupo de risco\n", this.getNome());
            this.setGrupo_de_risco(true);
        }else{
            if (this.isPandemia()){
                System.out.println("Não é do grupo de risco, mas deve respeitar a quarentena!");
                this.setGrupo_de_risco(false);
            }else{
                System.out.println("Lembre-se de lavar as mãos e beber água sempre!");
                this.setGrupo_de_risco(false);
            }
        }
    }
    
    public void acabouPandemia(boolean acabou){
        this.setPandemia(acabou);
    }

    public Pessoas(String nome, boolean pandemia, int idade, boolean doenca, boolean grupo_de_risco) {
        this.nome = nome;
        this.pandemia = pandemia;
        this.idade = idade;
        this.doenca = doenca;
        this.grupo_de_risco = grupo_de_risco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean isPandemia() {
        return pandemia;
    }

    public void setPandemia(boolean pandemia) {
        this.pandemia = pandemia;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public boolean isDoenca() {
        return doenca;
    }

    public void setDoenca(boolean doenca) {
        this.doenca = doenca;
    }

    public boolean isGrupo_de_risco() {
        return grupo_de_risco;
    }

    public void setGrupo_de_risco(boolean grupo_de_risco) {
        this.grupo_de_risco = grupo_de_risco;
    }
    
}
