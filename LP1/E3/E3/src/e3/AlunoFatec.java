package e3;


public class AlunoFatec {
    public String nome;
    private int ra;
    private String curso;
    private int semestre;
    
    public void terminarSemestre(){
        if (this.getSemestre() == 6){
            System.out.printf("Em tese o aluno %s se formou!\n", this.getNome());
        }else{
            this.setSemestre(this.getSemestre() + 1);
        } 
    }
    
    protected void programacao(){
        switch (this.getCurso()){
            case "Banco de Dados":
                System.out.println("Esse curso tem bastante matérias de programação!");
                break;
            case "Análise e Desenvolvimento de Sistemas":
                System.out.println("Esse curso tem bastante matérias de programação!");
                break;
            default:
                System.out.println("Esse curso (provavelmente) não tenha tantas matérias de programação!");
                break;
        }
    }
    
    public void status(){
        System.out.printf("Nome: %s", this.getNome());
        System.out.printf("\nR.A.: %d", this.getRa());
        System.out.printf("\nPais: %s", this.getCurso());
        this.programacao();
        System.out.printf("\nR.A.: %d", this.getSemestre());
        System.out.println("----------------------\n");        
    }

    public AlunoFatec(String nome, int ra, String curso, int semestre) {
        this.nome = nome;
        this.ra = ra;
        this.curso = curso;
        this.semestre = semestre;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getRa() {
        return ra;
    }

    public void setRa(int ra) {
        this.ra = ra;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public int getSemestre() {
        return semestre;
    }

    public void setSemestre(int semestre) {
        this.semestre = semestre;
    }
    
}
